package PresentationLayer;

import BusinessLayer.MenuItem;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class ChefGUI extends JFrame implements Observer {
    private JPanel panel=new JPanel();
    private JTextArea textarea=new JTextArea();

    public ChefGUI(){
        this.setSize(300,300);
        panel.add(textarea);
        this.setContentPane(panel);
        this.setVisible(true);
    }

    @Override
    public void update(Observable o, Object arg) {
        for(MenuItem i:(ArrayList<MenuItem>)arg)
            textarea.append(i.toString());
    }
}
