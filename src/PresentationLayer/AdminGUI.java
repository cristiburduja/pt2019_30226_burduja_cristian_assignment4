package PresentationLayer;

import BusinessLayer.BaseProduct;
import BusinessLayer.CompositeProduct;
import BusinessLayer.MenuItem;
import BusinessLayer.Restaurant;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AdminGUI extends JFrame{
    private JPanel panel=new JPanel();
    private JTable table;
    private DefaultTableModel tModel=new DefaultTableModel();
    private JScrollPane scrollPane=new JScrollPane();
    private JButton add=new JButton("Add item");
    private JButton delete=new JButton("Delete item");
    private JButton edit=new JButton("Edit item");
    private JTextField id=new JTextField("id",10);
    private JTextField name=new JTextField("name",10);
    private JTextField price=new JTextField("price",10);
    private JTextField ingredients=new JTextField("ingredients",20);

    private Restaurant restaurant=new Restaurant();
    public AdminGUI(){
        this.setSize(640,580);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        tModel.addColumn("Id");
        tModel.addColumn("Name");
        tModel.addColumn("Price");
        tModel.addColumn("Ingredients");
        populateTable();
        table=new JTable(tModel);
        add.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if(ingredients.getText().equals("") || ingredients.getText().equals("ingredients")){
                    BaseProduct product=new BaseProduct(Integer.valueOf(id.getText()),name.getText(),Double.valueOf(price.getText()));
                    restaurant.addProduct(product);
                    tModel.addRow(new Object[]{product.getId(),product.getName(),product.getPrice(),""});
                }
			else
                {
                    String[] idProducts=ingredients.getText().split(" ");
                    ArrayList<MenuItem> composite=new ArrayList<MenuItem>();
                    for(String s:idProducts){
                        composite.add(restaurant.getItemById(Integer.valueOf(s)));
                    }
                    CompositeProduct product=new CompositeProduct(Integer.valueOf(id.getText()),name.getText(),Double.valueOf(price.getText()),composite);
                    restaurant.addProduct(product);
                    String ingredients=new String();
                    for(MenuItem i:product.getIngredients())
                        ingredients+=i.getName() + " ";
                    tModel.addRow(new Object[]{product.getId(),product.getName(),product.getPrice(),ingredients});
                }
            }
        });
        delete.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                restaurant.deleteProduct(restaurant.getItemById(table.getSelectedRow()));
                tModel.removeRow(table.getSelectedRow());
            }
        });
        edit.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                add.doClick();
                delete.doClick();
            }
        });
        scrollPane=new JScrollPane(table);
        panel.add(id);
        panel.add(name);
        panel.add(price);
        panel.add(ingredients);
        panel.add(scrollPane);
        panel.add(add);
        panel.add(delete);
        panel.add(edit);

        this.setContentPane(panel);
        this.setVisible(true);
    }

    public void populateTable(){
        ArrayList<MenuItem> items=restaurant.getItems();
        for(MenuItem m:items){
            String ingredients=new String();
            for(MenuItem i:m.getIngredients())
                ingredients+=i.getName() + " ";
            tModel.addRow(new Object[]{m.getId(),m.getName(),m.getPrice(),ingredients});
        }
    }

}
