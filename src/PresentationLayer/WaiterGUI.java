package PresentationLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import BusinessLayer.Restaurant;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class WaiterGUI extends JFrame{

    private JPanel panel=new JPanel();
    private JTable table;
    private DefaultTableModel tModel=new DefaultTableModel();
    private JScrollPane scrollPane=new JScrollPane();
    private JTable orderTable;
    private DefaultTableModel otModel=new DefaultTableModel();
    private JScrollPane orderScrollPane=new JScrollPane();
    private JButton add=new JButton("Add Order");
    private JButton generate=new JButton("Generate Bill");
    private JTextField id=new JTextField("id");
    private JTextField date=new JTextField("date");
    private JTextField tableNr=new JTextField("table");

    private Restaurant restaurant=new Restaurant();
    public WaiterGUI(){
        this.setSize(640,980);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        tModel.addColumn("Id");
        tModel.addColumn("Name");
        tModel.addColumn("Price");
        tModel.addColumn("Ingredients");
        populateTable();
        otModel.addColumn("Order Id");
        otModel.addColumn("Date");
        otModel.addColumn("Table");

        table=new JTable(tModel);
        add.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                Order order=new Order(Integer.valueOf(id.getText()),date.getText(),Integer.valueOf(tableNr.getText()));
                ArrayList<MenuItem> orderedProducts=new ArrayList<MenuItem>();
                for(Integer i:table.getSelectedRows()){
                    orderedProducts.add(restaurant.getItemById(Integer.valueOf(String.valueOf(table.getValueAt(i,0)))));
                }
                restaurant.addOrder(order,orderedProducts);
                otModel.addRow(new Object[]{order.getOrderId(),order.getDate(),order.getTable()});
            }
        });
        generate.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                for(Order o:restaurant.getOrders().keySet()){
                    if(o.getOrderId()==Integer.valueOf(String.valueOf(table.getValueAt(table.getSelectedRow(),0)))) {
                        restaurant.generateBill(o);
                        break;
                    }
                }
            }
        });
        orderTable=new JTable(otModel);
        scrollPane=new JScrollPane(table);
        panel.add(id);
        panel.add(date);
        panel.add(tableNr);
        panel.add(scrollPane);
        orderScrollPane=new JScrollPane(orderTable);
        panel.add(orderScrollPane);
        panel.add(add);
        panel.add(generate);

        this.setContentPane(panel);
        this.setVisible(true);
    }

    public void populateTable(){
        ArrayList<MenuItem> items=restaurant.getItems();
        for(MenuItem m:items){
            String ingredients=new String();
            for(MenuItem i:m.getIngredients())
                ingredients+=i.getName() + " ";
            tModel.addRow(new Object[]{m.getId(),m.getName(),m.getPrice(),ingredients});
        }
    }

}
