package BusinessLayer;

import DataLayer.FileWriter;
import DataLayer.RestaurantSerializator;
import PresentationLayer.ChefGUI;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Observable;

public class Restaurant extends Observable implements IRestaurantProcessing {

    HashMap<Order, ArrayList<MenuItem>> orders=new HashMap<Order,ArrayList<MenuItem>>();
    FileWriter file=new FileWriter();
    RestaurantSerializator sez=new RestaurantSerializator();
    ArrayList<MenuItem> products=sez.deserialization();
    //ArrayList<MenuItem> products=new ArrayList<>();
    private static ChefGUI cg=new ChefGUI();

    public Restaurant(){
        addObserver(cg);
    }

    public void addProduct(MenuItem m){
        products.add(m);
        sez.serialization(products);
    }

    public void deleteProduct(MenuItem m){
        products.remove(m);
        sez.serialization(products);;
    }


    public void addOrder(Order o, ArrayList<MenuItem> p){
        orders.put(o,p);
        setChanged();
        notifyObservers(p);
    }

    public double computePrice(Order o){
        double price=0;
        for(MenuItem m:orders.get(o)){
            price+=m.computePrice();
        }
        return price;
    }

    public void generateBill(Order o){
        file.write(o,orders.get(o),computePrice(o));
    }

    public MenuItem getItemById(int id){
        for(MenuItem m:products){
            if(m.getId()==id)
                return m;
        }
        return null;
    }

    public ArrayList<MenuItem> getItems(){
        return products;
    }

    public HashMap<Order, ArrayList<MenuItem>> getOrders() {
        return orders;
    }
}
