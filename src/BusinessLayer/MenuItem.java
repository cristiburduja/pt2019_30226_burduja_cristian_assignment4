package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

public class MenuItem implements Serializable {
    private int id;
    private String name;
    private double price;
    private ArrayList<MenuItem> ingredients;

    public MenuItem(int id,String name,double price) {
        this.id=id;
        this.name = name;
        this.price=price;
    }

    public double computePrice(){
        return price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ArrayList<MenuItem> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<MenuItem> ingredients) {
        this.ingredients = ingredients;
    }

    @Override
    public String toString() {
        return "MenuItem{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", ingredients=" + ingredients +
                '}'+ "\n";
    }
}
