package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

public class CompositeProduct extends MenuItem implements Serializable {

    public CompositeProduct(int id,String name,double price, ArrayList<MenuItem> ingredients) {
        super(id,name,price);
        this.setIngredients(ingredients);
    }
    public double computePrice(){
        double totalPrice=this.getPrice();
        for(MenuItem ingredient:this.getIngredients()){
            totalPrice+=ingredient.computePrice();
        }
        return totalPrice;
    }

    @Override
    public String toString() {
        return "CompositeProduct{} " + super.toString()+ "\n";
    }
}
