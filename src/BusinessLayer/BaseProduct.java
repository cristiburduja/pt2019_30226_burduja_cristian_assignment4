package BusinessLayer;

import java.io.Serializable;
import java.util.ArrayList;

public class BaseProduct extends MenuItem implements Serializable {
    public BaseProduct(int id,String name,double price) {
        super(id,name,price);
        this.setIngredients(new ArrayList<MenuItem>());
    }

    @Override
    public String toString() {
        return "BaseProduct{} " + super.toString() + "\n";
    }
}
