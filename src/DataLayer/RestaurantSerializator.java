package DataLayer;

import BusinessLayer.MenuItem;

import java.io.*;
import java.util.ArrayList;

public class RestaurantSerializator {
    public void serialization(ArrayList<MenuItem> p)  {
        try
        {
            //Saving of object in a file
            FileOutputStream file = new FileOutputStream("serialization");
            ObjectOutputStream out = new ObjectOutputStream(file);

            // Method for serialization of object
            out.writeObject(p);

            out.close();
            file.close();

            System.out.println("Object has been serialized");

        }

        catch(IOException ex)
        {
            System.out.println("IOException is caught");
        } }


    public ArrayList<MenuItem> deserialization(){
        try
        {
            // Reading the object from a file
            FileInputStream file = new FileInputStream("serialization");
            ObjectInputStream in = new ObjectInputStream(file);

            // Method for deserialization of object
            ArrayList<MenuItem> p = (ArrayList<MenuItem>)in.readObject();

            in.close();
            file.close();
            if(p==null)
                return new ArrayList<>();
            return p;
        }

        catch(IOException ex)
        {
            ex.printStackTrace();
        }

        catch(ClassNotFoundException ex)
        {
            System.out.println("ClassNotFoundException is caught");
        }
        return null;
    }

}
