package DataLayer;

import BusinessLayer.MenuItem;
import BusinessLayer.Order;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;

public class FileWriter {

    public void write(Order o, ArrayList<MenuItem> p, double price){
        ArrayList<String> lines=new ArrayList<String>();
        lines.add(o.toString());
        lines.add(p.toString());
        lines.add(String.valueOf(price));
        Path file = Paths.get("bill.txt");
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
